test = [
    {
        'chain': 'eth',
        'chain_id': None,
        'has_supported_portfolio': True,
        'id': 'convex',
        'logo_url': 'https://static.debank.com/image/project/logo_url/convex/94040f96962532f0da4a0e5329296c4f.png',
        'name': 'Convex',
        'portfolio_item_list': [
            {
                'detail': {
                    'detail': None, 'proxy_detail': None
                },
                'detail_types': ['common'],
                'name': 'Farming',
                'stats': {
                    'asset_usd_value': 46578.325003754806,
                    'daily_cost_usd_value': 0.0,
                    'daily_net_yield_usd_value': 0.0,
                    'daily_yield_usd_value': 0.0,
                    'debt_usd_value': 0.0,
                    'net_usd_value': 46578.325003754806
                },
                'update_at': 1627457001.467294
            },
            {
                'detail': {
                    'detail': None, 'proxy_detail': None
                },
                'detail_types': ['common'],
                'name': 'Farming',
                'stats': {
                    'asset_usd_value': 85051.85119299042,
                    'daily_cost_usd_value': 0.0,
                    'daily_net_yield_usd_value': 0.0,
                    'daily_yield_usd_value': 0.0,
                        'debt_usd_value': 0.0,
                        'net_usd_value': 85051.85119299042
                },
                'update_at': 1627457001.631713
            }
        ],
        'site_url': 'https://www.convexfinance.com',
        'tvl': 0.0
    },
    {'chain': 'eth',
     'chain_id': None,
     'has_supported_portfolio': True,
     'id': 'harvest',
     'logo_url': 'https://static.debank.com/image/project/logo_url/harvest/18984eb91c5088fcc28724da737984b0.png',
     'name': 'Harvest',
     'portfolio_item_list': [{'detail': {'detail': None, 'proxy_detail': None},
                              'detail_types': ['common'],
                              'name': 'Farming',
                              'stats': {'asset_usd_value': 55452.127177702925,
                                        'daily_cost_usd_value': 0.0,
                                        'daily_net_yield_usd_value': 5.655191097420668,
                                        'daily_yield_usd_value': 5.655191097420668,
                                        'debt_usd_value': 0.0,
                                        'net_usd_value': 55452.127177702925},
                              'update_at': 1627457001.5954785},
                             {'detail': {'detail': None, 'proxy_detail': None},
                              'detail_types': ['common'],
                              'name': 'Farming',
                              'stats': {'asset_usd_value': 51807.478367620795,
                                        'daily_cost_usd_value': 0.0,
                                        'daily_net_yield_usd_value': 8.360316229925507,
                                        'daily_yield_usd_value': 8.360316229925507,
                                        'debt_usd_value': 0.0,
                                        'net_usd_value': 51807.478367620795},
                              'update_at': 1627457001.8454082},
                             {'detail': {'detail': None, 'proxy_detail': None},
                              'detail_types': ['common'],
                              'name': 'Farming',
                              'stats': {'asset_usd_value': 72797.65109738926,
                                        'daily_cost_usd_value': 0.0,
                                        'daily_net_yield_usd_value': 8.084816017260467,
                                        'daily_yield_usd_value': 8.084816017260467,
                                        'debt_usd_value': 0.0,
                                        'net_usd_value': 72797.65109738926},
                              'update_at': 1627457002.090359}],
     'site_url': 'https://harvest.finance',
     'tvl': 0.0}]
