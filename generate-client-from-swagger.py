#!/bin/env python3
"""
Generate DeBank OpenAPI client library for Python using swagger-codegen.
"""

import os


def remove_tests(tests_dir):
    print(f'Removing the `{tests_dir}` directory so that test file stubs are regenerated...')
    os.system(f'rm -rf {tests_dir}')


def generate_client(package_name, package_version, api_package_name, swagger_specfile):
    standard_opts = {
        'input-spec': swagger_specfile,
        'lang': "python",
        'output': '.',
        'api-package': api_package_name
    }
    java_opts = {
        'packageName': package_name,
        'packageVersion': package_version
    }

    # Format the options respective to the customs of their culture
    standard_options = [f'--{opt} {val}' for opt,val in standard_opts.items()]
    java_options = [f'{opt}="{val}"' for opt,val in java_opts.items()]

    # Conglomerate the options by paying homage to their ancestors
    swagger_gen_command = " ".join([
        'swagger-codegen generate',
        f'{" ".join(standard_options)}',
        f'-D{",".join(java_options)}'
    ])

    print(f'Running `{swagger_gen_command}`...')
    os.system(swagger_gen_command)


def main():
    tests_dir = './test'
    package_name = 'debank'
    package_version = '0.1.3'
    api_package_name = 'openapi'
    swagger_specfile = f'./{package_name}_{api_package_name}-swagger.json'

    remove_tests(tests_dir)
    generate_client(package_name, package_version, api_package_name, swagger_specfile)


if __name__ == '__main__':
    main()
