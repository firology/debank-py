# ProtocolTVL

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_at** | **str** | %Y-%m-%d format date of the date  | [optional] 
**value** | **float** | amount of tvl on a specified date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

