# TokenSpend

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **object** |  | [optional] 
**value** | **object** |  | [optional] 
**exposure_usd** | **object** |  | [optional] 
**protocol** | [**ProtocolBasic**](ProtocolBasic.md) |  | [optional] 
**is_contract** | **object** |  | [optional] 
**is_open_source** | **object** |  | [optional] 
**is_hacked** | **object** |  | [optional] 
**is_abandoned** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

