# PortfolioItemDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supply_token_list** | [**list[TokenBalance]**](TokenBalance.md) |  | [optional] 
**reward_token_list** | [**list[TokenBalance]**](TokenBalance.md) |  | [optional] 
**borrow_token_list** | [**list[TokenBalance]**](TokenBalance.md) |  | [optional] 
**health_rate** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

