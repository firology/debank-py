# debank.WalletApi

All URIs are relative to *https://openapi.debank.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_check_origin**](WalletApi.md#get_check_origin) | **GET** /v1/wallet/check_origin | TODO: Response not yet defined
[**get_ens**](WalletApi.md#get_ens) | **GET** /v1/wallet/ens | TODO: Response not yet defined
[**get_explain_origin**](WalletApi.md#get_explain_origin) | **GET** /v1/wallet/explain_origin | TODO: Response not yet defined
[**get_explain_tx**](WalletApi.md#get_explain_tx) | **GET** /v1/wallet/get_tx | TODO: Response not yet defined
[**get_gas_market**](WalletApi.md#get_gas_market) | **GET** /v1/wallet/gas_market | TODO: Response not yet defined
[**get_pending_tx_count**](WalletApi.md#get_pending_tx_count) | **GET** /v1/wallet/pending_tx_count | TODO: Response not yet defined
[**get_recommend_chains**](WalletApi.md#get_recommend_chains) | **GET** /v1/wallet/recommend_chains | TODO: Response not yet defined
[**get_support_chains**](WalletApi.md#get_support_chains) | **GET** /v1/wallet/supported_chains | TODO: Response not yet defined
[**get_url_config**](WalletApi.md#get_url_config) | **GET** /v1/wallet/config | TODO: Response not yet defined
[**post_check_text**](WalletApi.md#post_check_text) | **POST** /v1/wallet/check_text | TODO: Response not yet defined
[**post_check_tx**](WalletApi.md#post_check_tx) | **POST** /v1/wallet/check_tx | TODO: Response not yet defined
[**post_eth_call**](WalletApi.md#post_eth_call) | **POST** /v1/wallet/eth_rpc | TODO: Response not yet defined
[**post_explain_text**](WalletApi.md#post_explain_text) | **POST** /v1/wallet/explain_text | TODO: Response not yet defined
[**post_explain_tx**](WalletApi.md#post_explain_tx) | **POST** /v1/wallet/explain_tx | TODO: Response not yet defined
[**post_push_tx**](WalletApi.md#post_push_tx) | **POST** /v1/wallet/push_tx | TODO: Response not yet defined

# **get_check_origin**
> get_check_origin(user_addr, origin)

TODO: Response not yet defined

check origin

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()
user_addr = 'user_addr_example' # str | User Address
origin = 'origin_example' # str | DApp WebHost

try:
    # TODO: Response not yet defined
    api_instance.get_check_origin(user_addr, origin)
except ApiException as e:
    print("Exception when calling WalletApi->get_check_origin: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_addr** | **str**| User Address | 
 **origin** | **str**| DApp WebHost | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ens**
> get_ens(text)

TODO: Response not yet defined

ens

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()
text = 'text_example' # str | text

try:
    # TODO: Response not yet defined
    api_instance.get_ens(text)
except ApiException as e:
    print("Exception when calling WalletApi->get_ens: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **text** | **str**| text | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_explain_origin**
> get_explain_origin(user_addr, origin)

TODO: Response not yet defined

explain origin

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()
user_addr = 'user_addr_example' # str | User Address
origin = 'origin_example' # str | DApp WebHost

try:
    # TODO: Response not yet defined
    api_instance.get_explain_origin(user_addr, origin)
except ApiException as e:
    print("Exception when calling WalletApi->get_explain_origin: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_addr** | **str**| User Address | 
 **origin** | **str**| DApp WebHost | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_explain_tx**
> get_explain_tx(chain_id, tx_id, gas_price)

TODO: Response not yet defined

get tx

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()
chain_id = 'chain_id_example' # str | ChainId
tx_id = 'tx_id_example' # str | tx hash id
gas_price = 'gas_price_example' # str | gasPrice

try:
    # TODO: Response not yet defined
    api_instance.get_explain_tx(chain_id, tx_id, gas_price)
except ApiException as e:
    print("Exception when calling WalletApi->get_explain_tx: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chain_id** | **str**| ChainId | 
 **tx_id** | **str**| tx hash id | 
 **gas_price** | **str**| gasPrice | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_gas_market**
> get_gas_market(chain_id, custom_price=custom_price)

TODO: Response not yet defined

gas market

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()
chain_id = 'chain_id_example' # str | ChainId
custom_price = '0' # str | CustomPrice (optional) (default to 0)

try:
    # TODO: Response not yet defined
    api_instance.get_gas_market(chain_id, custom_price=custom_price)
except ApiException as e:
    print("Exception when calling WalletApi->get_gas_market: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chain_id** | **str**| ChainId | 
 **custom_price** | **str**| CustomPrice | [optional] [default to 0]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_pending_tx_count**
> get_pending_tx_count(user_addr)

TODO: Response not yet defined

user pending tx count

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()
user_addr = 'user_addr_example' # str | User Address

try:
    # TODO: Response not yet defined
    api_instance.get_pending_tx_count(user_addr)
except ApiException as e:
    print("Exception when calling WalletApi->get_pending_tx_count: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_addr** | **str**| User Address | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_recommend_chains**
> get_recommend_chains(user_addr, origin)

TODO: Response not yet defined

recommend chains

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()
user_addr = 'user_addr_example' # str | User Address
origin = 'origin_example' # str | DApp WebHost

try:
    # TODO: Response not yet defined
    api_instance.get_recommend_chains(user_addr, origin)
except ApiException as e:
    print("Exception when calling WalletApi->get_recommend_chains: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_addr** | **str**| User Address | 
 **origin** | **str**| DApp WebHost | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_support_chains**
> get_support_chains()

TODO: Response not yet defined

wallet support chain

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.get_support_chains()
except ApiException as e:
    print("Exception when calling WalletApi->get_support_chains: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_url_config**
> get_url_config()

TODO: Response not yet defined

wallet api config

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.get_url_config()
except ApiException as e:
    print("Exception when calling WalletApi->get_url_config: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_check_text**
> post_check_text()

TODO: Response not yet defined

check text

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.post_check_text()
except ApiException as e:
    print("Exception when calling WalletApi->post_check_text: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_check_tx**
> post_check_tx()

TODO: Response not yet defined

check tx

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.post_check_tx()
except ApiException as e:
    print("Exception when calling WalletApi->post_check_tx: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_eth_call**
> post_eth_call()

TODO: Response not yet defined

eth rpc

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.post_eth_call()
except ApiException as e:
    print("Exception when calling WalletApi->post_eth_call: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_explain_text**
> post_explain_text()

TODO: Response not yet defined

explain text

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.post_explain_text()
except ApiException as e:
    print("Exception when calling WalletApi->post_explain_text: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_explain_tx**
> post_explain_tx()

TODO: Response not yet defined

explain tx

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.post_explain_tx()
except ApiException as e:
    print("Exception when calling WalletApi->post_explain_tx: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_push_tx**
> post_push_tx()

TODO: Response not yet defined

push tx

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.WalletApi()

try:
    # TODO: Response not yet defined
    api_instance.post_push_tx()
except ApiException as e:
    print("Exception when calling WalletApi->post_push_tx: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

