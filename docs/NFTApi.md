# debank.NFTApi

All URIs are relative to *https://openapi.debank.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_collection_list**](NFTApi.md#get_collection_list) | **GET** /v1/nft/collections | 

# **get_collection_list**
> list[NFT] get_collection_list()



### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.NFTApi()

try:
    api_response = api_instance.get_collection_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling NFTApi->get_collection_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[NFT]**](NFT.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

