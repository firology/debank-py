# debank.ProtocolApi

All URIs are relative to *https://openapi.debank.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_protocol_list_r**](ProtocolApi.md#get_protocol_list_r) | **GET** /v1/protocol/list | 
[**get_protocol_r**](ProtocolApi.md#get_protocol_r) | **GET** /v1/protocol | 
[**get_tvl**](ProtocolApi.md#get_tvl) | **GET** /v1/protocol/tvl | FIXME: Seems broken (returns 400 bad req).

# **get_protocol_list_r**
> list[Protocol] get_protocol_list_r()



### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.ProtocolApi()

try:
    api_response = api_instance.get_protocol_list_r()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProtocolApi->get_protocol_list_r: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Protocol]**](Protocol.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_protocol_r**
> Protocol get_protocol_r(id)



### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.ProtocolApi()
id = 'id_example' # str | protocol id

try:
    api_response = api_instance.get_protocol_r(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProtocolApi->get_protocol_r: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| protocol id | 

### Return type

[**Protocol**](Protocol.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tvl**
> list[ProtocolTVL] get_tvl(id)

FIXME: Seems broken (returns 400 bad req).

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.ProtocolApi()
id = 'id_example' # str | protocolID

try:
    # FIXME: Seems broken (returns 400 bad req).
    api_response = api_instance.get_tvl(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProtocolApi->get_tvl: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| protocolID | 

### Return type

[**list[ProtocolTVL]**](ProtocolTVL.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

