# ChainBalance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**usd_value** | **float** | Chain balance value in US Dollars | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

