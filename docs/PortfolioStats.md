# PortfolioStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_usd_value** | **float** | value of asset in USD | [optional] 
**debt_usd_value** | **float** | value of debt in USD | [optional] 
**net_usd_value** | **float** | net value in USD | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

