# ProtocolBalance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain** | **str** | Chain | [optional] 
**tvl** | **float** | Total value locked | [optional] 
**net_usd_value** | **float** | Net value in US Dollars | [optional] 
**asset_usd_value** | **float** | Asset value in US Dollars | [optional] 
**debt_usd_value** | **float** | Debt value in US Dollars | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

