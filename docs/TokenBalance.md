# TokenBalance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain** | **str** | Chain | [optional] 
**display_symbol** | **str** |  | [optional] 
**optimized_symbol** | **str** |  | [optional] 
**is_core** | **bool** |  | [optional] 
**time_at** | **float** |  | [optional] 
**amount** | **float** | Balance native amount | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

