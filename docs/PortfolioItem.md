# PortfolioItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stats** | [**PortfolioStats**](PortfolioStats.md) |  | [optional] 
**update_at** | **float** |  | [optional] 
**name** | **str** |  | [optional] 
**pool_id** | **str** |  | [optional] 
**detail_types** | **list[str]** | ? | [optional] 
**detail** | [**PortfolioItemDetail**](PortfolioItemDetail.md) |  | [optional] 
**proxy_detail** | **dict(str, str)** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

