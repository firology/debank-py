# Protocol

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain_id** | **int** | ChainID | [optional] 
**site_url** | **str** | Prioritize websites that can be interacted with, not official websites | [optional] 
**has_supported_portfolio** | **bool** | Is portfolio already supported | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

