# ProtocolBalanceComplex

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain** | **str** | Chain | [optional] 
**tvl** | **float** | Total value locked | [optional] 
**portfolio_item_list** | [**list[PortfolioItem]**](PortfolioItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

