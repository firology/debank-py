# ProtocolBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | protocol id | [optional] 
**chain** | **str** | Chain | [optional] 
**name** | **str** |  | [optional] 
**logo_url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

