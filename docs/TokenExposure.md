# TokenExposure

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain** | **str** | Chain | [optional] 
**balance** | **float** | Balance amount | [optional] 
**sum_exposure_usd** | **float** | Balance amount | [optional] 
**exposure_balance** | **float** | Balance amount | [optional] 
**spenders** | [**list[TokenSpend]**](TokenSpend.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

