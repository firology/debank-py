# debank.UserApi

All URIs are relative to *https://openapi.debank.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_user_chain_balance**](UserApi.md#get_user_chain_balance) | **GET** /v1/user/chain_balance | 
[**get_user_complex_protocol_list**](UserApi.md#get_user_complex_protocol_list) | **GET** /v1/user/complex_protocol_list | 
[**get_user_nft_list**](UserApi.md#get_user_nft_list) | **GET** /v1/user/nft_list | TODO: Response not yet defined
[**get_user_protocol**](UserApi.md#get_user_protocol) | **GET** /v1/user/protocol | 
[**get_user_simple_protocol_list**](UserApi.md#get_user_simple_protocol_list) | **GET** /v1/user/simple_protocol_list | 
[**get_user_token**](UserApi.md#get_user_token) | **GET** /v1/user/token | TODO: Response not yet defined
[**get_user_token_authorized_list**](UserApi.md#get_user_token_authorized_list) | **GET** /v1/user/token_authorized_list | 
[**get_user_token_list**](UserApi.md#get_user_token_list) | **GET** /v1/user/token_list | 
[**get_user_token_search**](UserApi.md#get_user_token_search) | **GET** /v1/user/token_search | TODO: Response not yet defined
[**get_user_total_balance**](UserApi.md#get_user_total_balance) | **GET** /v1/user/total_balance | 

# **get_user_chain_balance**
> Balance get_user_chain_balance(id, chain_id)



Get the net assets of a chain

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | User Address
chain_id = 'chain_id_example' # str | ChainID

try:
    api_response = api_instance.get_user_chain_balance(id, chain_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->get_user_chain_balance: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| User Address | 
 **chain_id** | **str**| ChainID | 

### Return type

[**Balance**](Balance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_complex_protocol_list**
> list[ProtocolBalanceComplex] get_user_complex_protocol_list(id, chain_id=chain_id)



Get list of protocols with user portfolio details

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | User Address
chain_id = 'chain_id_example' # str | ChainID (optional)

try:
    api_response = api_instance.get_user_complex_protocol_list(id, chain_id=chain_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->get_user_complex_protocol_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| User Address | 
 **chain_id** | **str**| ChainID | [optional] 

### Return type

[**list[ProtocolBalanceComplex]**](ProtocolBalanceComplex.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_nft_list**
> get_user_nft_list(id, chain_id=chain_id)

TODO: Response not yet defined

Get user nft list

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | Address
chain_id = 'chain_id_example' # str | ChainID (optional)

try:
    # TODO: Response not yet defined
    api_instance.get_user_nft_list(id, chain_id=chain_id)
except ApiException as e:
    print("Exception when calling UserApi->get_user_nft_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Address | 
 **chain_id** | **str**| ChainID | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_protocol**
> ProtocolBalanceComplex get_user_protocol(id, protocol_id)



Get the user's portfolio in the protocol

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | User Address
protocol_id = 'protocol_id_example' # str | protocol id

try:
    api_response = api_instance.get_user_protocol(id, protocol_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->get_user_protocol: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| User Address | 
 **protocol_id** | **str**| protocol id | 

### Return type

[**ProtocolBalanceComplex**](ProtocolBalanceComplex.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_simple_protocol_list**
> list[ProtocolBalance] get_user_simple_protocol_list(id, chain_id=chain_id)



Stats the user's protocol assets on a chain

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | User Address
chain_id = 'chain_id_example' # str | ChainID (optional)

try:
    api_response = api_instance.get_user_simple_protocol_list(id, chain_id=chain_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->get_user_simple_protocol_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| User Address | 
 **chain_id** | **str**| ChainID | [optional] 

### Return type

[**list[ProtocolBalance]**](ProtocolBalance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_token**
> get_user_token(id, token_id, chain_id=chain_id)

TODO: Response not yet defined

Get user token balance

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | Address
token_id = 'token_id_example' # str | token id
chain_id = 'chain_id_example' # str | ChainID (optional)

try:
    # TODO: Response not yet defined
    api_instance.get_user_token(id, token_id, chain_id=chain_id)
except ApiException as e:
    print("Exception when calling UserApi->get_user_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Address | 
 **token_id** | **str**| token id | 
 **chain_id** | **str**| ChainID | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_token_authorized_list**
> list[TokenExposure] get_user_token_authorized_list(id, chain_id)



Show the user's risk exposure of approved token on a chain

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | User Address
chain_id = 'chain_id_example' # str | ChainID

try:
    api_response = api_instance.get_user_token_authorized_list(id, chain_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->get_user_token_authorized_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| User Address | 
 **chain_id** | **str**| ChainID | 

### Return type

[**list[TokenExposure]**](TokenExposure.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_token_list**
> list[TokenBalance] get_user_token_list(id, chain_id=chain_id, is_all=is_all, has_balance=has_balance)



Get user token balance

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | Address
chain_id = 'chain_id_example' # str | ChainID (optional)
is_all = true # bool | If true, all tokens are returned, including protocol-derived tokens (optional) (default to true)
has_balance = true # bool | If true, only token with balance will returned (optional) (default to true)

try:
    api_response = api_instance.get_user_token_list(id, chain_id=chain_id, is_all=is_all, has_balance=has_balance)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->get_user_token_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Address | 
 **chain_id** | **str**| ChainID | [optional] 
 **is_all** | **bool**| If true, all tokens are returned, including protocol-derived tokens | [optional] [default to true]
 **has_balance** | **bool**| If true, only token with balance will returned | [optional] [default to true]

### Return type

[**list[TokenBalance]**](TokenBalance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_token_search**
> get_user_token_search(id, q, chain_id=chain_id, has_balance=has_balance)

TODO: Response not yet defined

Get Tokens By Filter

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | User Address
q = 'q_example' # str | filter args
chain_id = 'chain_id_example' # str | ChainID (optional)
has_balance = true # bool | If true, only token with balance will returned (optional) (default to true)

try:
    # TODO: Response not yet defined
    api_instance.get_user_token_search(id, q, chain_id=chain_id, has_balance=has_balance)
except ApiException as e:
    print("Exception when calling UserApi->get_user_token_search: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| User Address | 
 **q** | **str**| filter args | 
 **chain_id** | **str**| ChainID | [optional] 
 **has_balance** | **bool**| If true, only token with balance will returned | [optional] [default to true]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user_total_balance**
> InlineResponse200 get_user_total_balance(id)



Get net assets on multiple chains, including tokens and protocols

### Example
```python
from __future__ import print_function
import time
import debank
from debank.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = debank.UserApi()
id = 'id_example' # str | User Address

try:
    api_response = api_instance.get_user_total_balance(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->get_user_total_balance: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| User Address | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

