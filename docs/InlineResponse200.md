# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_usd_value** | **float** | Total balance of account in US Dollars | [optional] 
**chain_list** | [**list[ChainBalance]**](ChainBalance.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

