# NFT

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Collection ID | [optional] 
**name** | **str** |  | [optional] 
**logo_url** | **str** |  | [optional] 
**is_core** | **bool** |  | [optional] 
**create_at** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

