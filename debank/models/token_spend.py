# coding: utf-8

"""
    DeBank OpenAPI

    Build for DeFi Developers.  # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class TokenSpend(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'object',
        'value': 'object',
        'exposure_usd': 'object',
        'protocol': 'ProtocolBasic',
        'is_contract': 'object',
        'is_open_source': 'object',
        'is_hacked': 'object',
        'is_abandoned': 'object'
    }

    attribute_map = {
        'id': 'id',
        'value': 'value',
        'exposure_usd': 'exposure_usd',
        'protocol': 'protocol',
        'is_contract': 'is_contract',
        'is_open_source': 'is_open_source',
        'is_hacked': 'is_hacked',
        'is_abandoned': 'is_abandoned'
    }

    def __init__(self, id=None, value=None, exposure_usd=None, protocol=None, is_contract=None, is_open_source=None, is_hacked=None, is_abandoned=None):  # noqa: E501
        """TokenSpend - a model defined in Swagger"""  # noqa: E501
        self._id = None
        self._value = None
        self._exposure_usd = None
        self._protocol = None
        self._is_contract = None
        self._is_open_source = None
        self._is_hacked = None
        self._is_abandoned = None
        self.discriminator = None
        if id is not None:
            self.id = id
        if value is not None:
            self.value = value
        if exposure_usd is not None:
            self.exposure_usd = exposure_usd
        if protocol is not None:
            self.protocol = protocol
        if is_contract is not None:
            self.is_contract = is_contract
        if is_open_source is not None:
            self.is_open_source = is_open_source
        if is_hacked is not None:
            self.is_hacked = is_hacked
        if is_abandoned is not None:
            self.is_abandoned = is_abandoned

    @property
    def id(self):
        """Gets the id of this TokenSpend.  # noqa: E501


        :return: The id of this TokenSpend.  # noqa: E501
        :rtype: object
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this TokenSpend.


        :param id: The id of this TokenSpend.  # noqa: E501
        :type: object
        """

        self._id = id

    @property
    def value(self):
        """Gets the value of this TokenSpend.  # noqa: E501


        :return: The value of this TokenSpend.  # noqa: E501
        :rtype: object
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this TokenSpend.


        :param value: The value of this TokenSpend.  # noqa: E501
        :type: object
        """

        self._value = value

    @property
    def exposure_usd(self):
        """Gets the exposure_usd of this TokenSpend.  # noqa: E501


        :return: The exposure_usd of this TokenSpend.  # noqa: E501
        :rtype: object
        """
        return self._exposure_usd

    @exposure_usd.setter
    def exposure_usd(self, exposure_usd):
        """Sets the exposure_usd of this TokenSpend.


        :param exposure_usd: The exposure_usd of this TokenSpend.  # noqa: E501
        :type: object
        """

        self._exposure_usd = exposure_usd

    @property
    def protocol(self):
        """Gets the protocol of this TokenSpend.  # noqa: E501


        :return: The protocol of this TokenSpend.  # noqa: E501
        :rtype: ProtocolBasic
        """
        return self._protocol

    @protocol.setter
    def protocol(self, protocol):
        """Sets the protocol of this TokenSpend.


        :param protocol: The protocol of this TokenSpend.  # noqa: E501
        :type: ProtocolBasic
        """

        self._protocol = protocol

    @property
    def is_contract(self):
        """Gets the is_contract of this TokenSpend.  # noqa: E501


        :return: The is_contract of this TokenSpend.  # noqa: E501
        :rtype: object
        """
        return self._is_contract

    @is_contract.setter
    def is_contract(self, is_contract):
        """Sets the is_contract of this TokenSpend.


        :param is_contract: The is_contract of this TokenSpend.  # noqa: E501
        :type: object
        """

        self._is_contract = is_contract

    @property
    def is_open_source(self):
        """Gets the is_open_source of this TokenSpend.  # noqa: E501


        :return: The is_open_source of this TokenSpend.  # noqa: E501
        :rtype: object
        """
        return self._is_open_source

    @is_open_source.setter
    def is_open_source(self, is_open_source):
        """Sets the is_open_source of this TokenSpend.


        :param is_open_source: The is_open_source of this TokenSpend.  # noqa: E501
        :type: object
        """

        self._is_open_source = is_open_source

    @property
    def is_hacked(self):
        """Gets the is_hacked of this TokenSpend.  # noqa: E501


        :return: The is_hacked of this TokenSpend.  # noqa: E501
        :rtype: object
        """
        return self._is_hacked

    @is_hacked.setter
    def is_hacked(self, is_hacked):
        """Sets the is_hacked of this TokenSpend.


        :param is_hacked: The is_hacked of this TokenSpend.  # noqa: E501
        :type: object
        """

        self._is_hacked = is_hacked

    @property
    def is_abandoned(self):
        """Gets the is_abandoned of this TokenSpend.  # noqa: E501


        :return: The is_abandoned of this TokenSpend.  # noqa: E501
        :rtype: object
        """
        return self._is_abandoned

    @is_abandoned.setter
    def is_abandoned(self, is_abandoned):
        """Sets the is_abandoned of this TokenSpend.


        :param is_abandoned: The is_abandoned of this TokenSpend.  # noqa: E501
        :type: object
        """

        self._is_abandoned = is_abandoned

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(TokenSpend, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, TokenSpend):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
