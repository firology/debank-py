# coding: utf-8

"""
    DeBank OpenAPI

    Build for DeFi Developers.  # noqa: E501

    OpenAPI spec version: 1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import debank
from debank.models.chain_balance import ChainBalance  # noqa: E501
from debank.rest import ApiException


class TestChainBalance(unittest.TestCase):
    """ChainBalance unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testChainBalance(self):
        """Test ChainBalance"""
        # FIXME: construct object with mandatory attributes with example values
        # model = debank.models.chain_balance.ChainBalance()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
