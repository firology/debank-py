# DeBank OpenAPI
This Python package is based on the [DeBank OpenAPI](https://openapi.debank.com/docs) Swagger definition. Several changes were made to the original [swagger.json](https://openapi.debank.com/swagger.json) file in order to generate the client correctly. Mainly, the response schemas were missing for most of the endpoints so some config was added and some model definitions were created so that the data returned can be used as Python objects.

## License
The original DeBank OpenAPI swagger file was produced by DeBank but no license information was included in the original file. The rest of this is under the [MIT License](LICENSE.md).

# Contributing
Contributions are very welcome! Please see the [README](README.md) where there are items marked `FIXME` or `TODO` for some ideas on where to start.

## Adding new parts of the API
Just update the [swagger json file](debank_openapi-swagger.json) according to the [Swagger 2.0 spec](https://swagger.io/specification/v2/).

### Generating the Python client
After updating the swagger definition, regenerate the client with the command:

```shell
python3 generate-client-from-swagger.py
```

This will first delete the `./test/` directory so that the test stubs will be regenerated. Then it runs `swagger-codegen generate` with options appropriate for this Python package. Refer to the code in the [generator script](generate-client-from-swagger.py) itself for more details on the options it passes to `swagger-codegen`.

# Thanks!
